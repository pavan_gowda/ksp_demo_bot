#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: get_config.py
# Description: Script to get config objects# Date: 11-01-2021
import sys

sys.path.append('../')

from db.tracker_store_connection import load_config, RedisDB
from raven import Client
import json
from utils.text_format import color

from logger_conf.logger import get_logger
from logger_conf.logger import CustomAdapter

from datetime import date

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])
client_flag = gen_config["raven"]["flag"]
# redis connection
redis_db_obj = RedisDB()

with open("../configs/templates.json", "r", encoding="utf-8") as temp:
    templates = json.load(temp)

def capture_exception():
    logger.info("Client flag is :"+str(client_flag))
    logger.info(client_flag)
    if client_flag:
        return client.captureException()
    else:
        return None

def log_redis_update(update_flag, action_name, s_id):
    logger.info("\n   ")
    if update_flag is True:
        logger.info("Updated stage in redis {}".format(action_name),sender_id=s_id)
    else:
        logger.info("failed to update in redis {}".format(action_name),sender_id=s_id)
    logger.info("\n   ")


def reform_date(string_date):
    if "/" in string_date:
        string_date = string_date.split("/")
        new_date = date(day=int(string_date[0]), month=int(string_date[1]), year=int(string_date[2])).strftime('%d %B %Y')
        new_date = new_date.lower()
        for eng, hin in hindi_dict_mapping.items():
            eng = eng.lower()
            new_date = new_date.replace(eng,hin)
    else:
        new_date = string_date
    return new_date

def get_replaced_message(template_message, sender_id, cust_info):
    """function to get value in string form of a key from redis

    Args:
        template_messgae (string): message from templates.json
        sender_id (string): sender id of the conversation
        cust_info (dictionary): cust info from dictionary

    Returns:
        string: actual template message to be uttered
    """
    try:
        prepaymentAmount = str(cust_info["prepaymentAmount"])
        oldDeliveryDate = str(cust_info["oldDeliveryDate"])
        newDeliveryDate = str(cust_info["newDeliveryDate"])
        deliveryDate = str(cust_info["deliveryDate"])
        name = str(cust_info["full_name"])
        return_Action_TAT = str(cust_info["return_Action_TAT"])

        Order_ID = str(cust_info["Order_ID"])
        Last_Reattempt_Reason = str(cust_info["Last_Reattempt_Reason"])
        Order_Delivery_Rescheduled_Date = str(cust_info["Order_Delivery_Rescheduled_Date"])
        RTO_Reason = str(cust_info["RTO_Reason"])
        Delivery_promise_date = str(cust_info["Delivery_promise_date"])
        promiseDateFor2days = str(cust_info["promiseDateFor2days"])
        Uploaded_Date_plus2 = str(cust_info["Uploaded_Date_plus2"])
        RefundAmount = str(cust_info["RefundAmount"])

            
        # amount_due = num2audios(int(amount_due))
        # late_fee = num2audios(int(late_fee))       
        # due_date  = covert_date_string(due_date)

        logger.info("Inside method: get_replaced_message")


        # prepaymentAmount
        # oldDeliveryDate
        # newDeliveryDate
        # deliveryDate 11
        # return Action TAT
        # Order ID
        # Last Reattempt Reason
        # #Uploaded Date + 2 days
        # Order Delivery Rescheduled Date
        # RTO Reason
        # Delivery promise date
        # promiseDateFor2days

        variable_replacement_map = {
            "name": name,
            "prepaymentAmount": prepaymentAmount,
            "oldDeliveryDate": oldDeliveryDate,
            "newDeliveryDate" : newDeliveryDate,
            "full_name": name,
            "deliveryDate":deliveryDate,
            "return_Action_TAT" : return_Action_TAT,
            "Order_ID" : Order_ID,
            "Last_Reattempt_Reason":Last_Reattempt_Reason,
            "Uploaded_Date_plus2": Uploaded_Date_plus2,
            "Order_Delivery_Rescheduled_Date":Order_Delivery_Rescheduled_Date,
            "RTO_Reason":RTO_Reason,
            "Delivery_promise_date":Delivery_promise_date,
            "promiseDateFor2days":promiseDateFor2days,
            "RefundAmount":RefundAmount

        }

        # replaced_date = reform_date(str(due_date))
        # replace the variable name in the templates with variable value extracted from db
        logger.info("\n variable_replacement_map: "+ str(variable_replacement_map))

        for var_name, actual_variable in variable_replacement_map.items():
            logger.info(var_name +  actual_variable)
            template_message = template_message.replace(var_name,str(actual_variable))

        # template_message = template_message.replace("var_due_date",replaced_date)
        logger.info("\n template_message: "+ str(template_message))

    except Exception as e:
        logger.info("Exception in dynamically changing the template: "+ str(e),sender_id=sender_id)
        capture_exception()

    return template_message
