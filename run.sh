#! /bin/bash
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`
LOGS=OLD_LOGS
CONVO_LOG=logs
if [ ! -d "$LOGS" ]; then
    mkdir $LOGS
fi
if [ ! -d "$CONVO_LOG" ]; then
    mkdir $CONVO_LOG
fi
pkill -f -9 python3
export LANG=C.UTF-8
export TWILIO_ACCOUNT_SID=ACfe34dfb4c66ff690a0104d02a4481089
export TWILIO_AUTH_TOKEN=4e90399bacd426d050a4c462fb13f04f
export TWILIO_NUMBER=whatsapp:+14155238886
cd main/
mv nohup.out ../OLD_LOGS/nohup.out-${DATE}.bck
nohup rasa run actions >> nohup.out 2>&1 &
nohup python3 rasa_server.py >> nohup.out 2>&1 &

