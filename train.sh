rasa train --fixed-model-name zestmoney_chatbot --config ./configs/config.yml --domain ./configs/domain.yml
mkdir -p models
cd ./models
tar xvf zestmoney_chatbot.tar.gz
