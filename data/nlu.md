## intent:greet
- Hello 
- Hi 
- Hey 
- Hello 
- Hello there 
- Hi there 
- Hello Hello 
- Hi Hi 
- Namaste
- Namaskar
- Hello Good Morning 
- Hi Good Morning 
- Hey good morning 
- Hello Good Morning madam
- Hello good morning ma’am
- Hello Good Morning Sir
- Hi Sir
- Good morning 
- Hello Good Morning 
- Hello Hello Good Morning 
- Hi Hello Good Morning 
- Halu Good Morning 
- Namaste Good Morning 
- Hello Good Morning 
- Hello good morning 
- Hello Good Afternoon 
- Hi Good Afternoon 
- Hey good afternoon 
- Hello Good Afternoon 
- Good afternoon 
- Hello Hello Good Afternoon 
- Hi Hello Good Afternoon 
- Namaste Good Afternoon 
- Hello Hello Good Afternoon 
- Halovs Good Afternoon 
- Hello Good Afternoon 
- Hello Good Evening 
- Hi Good Evening 
- Hey good evening 
- Hello Good Evening 
- Hello Good Evening 
- Good Evening 
- Hello Hello Good Evening
- Yo


## intent:select_theft
- 1
- One
- Ondu
- Number 1
- Theft
- Thief
- Stolen
- Mobile stolen
- No 1
- Num 1
- Digit 1

## intent:select_passport
- 2
- Two
- Yeradu
- Number 2
- Passport
- Applying passport
- Need passport
- No 2
- Num 2
- Digit 2

## intent:new_theft
- 1
- One
- Ondu
- Number 1
- New Theft
- First time Theft
- No 1
- Num 1
- Digit 1
- New user
- New
- I am new user only
- New sir
- New madam
- New mam
- New theft

## intent:existing_theft
- 2
- Two
- Yeradu
- Number 2
- Passport
- Applying passport
- Need passport
- No 2
- Num 2
- Digit 2
- Existing
- Existing user
- Not New user
- Old user

## intent:phone_number_theft
- 9902466956
- My number is 7201465984
- Mobile number is 1234567898
- phone number 4567891235

## intent:imei_theft
- IMEI456123789
- IMEI456123787
- IMEI456123780

## intent:location_theft
- Basavangudi
- Banaswadi
- Banshankari

## intent:docket_theft
- IN-KAR-BLR753123
- IN-KAR-BLR753456
- IN-KAR-BLR753789
- IN-KAR-BLR753000
- IN-KAR-BLR12753456


## intent:new_passport
- 1
- One
- Ondu
- Number 1
- No 1
- Num 1
- Digit 1
- First time
- 1st time
- One time
- New Passport
- Applying for the first time


## intent:existing_passport
- 2
- Two
- Yeradu
- Number 2
- No 2
- Num 2
- Digit 2
- Already applied
- Applied
- Not first time
- 2nd time
- Second time
- Third time

## intent:renewal_passport
- 1
- One
- Ondu
- Number 1
- No 1
- Num 1
- Digit 1
- Renewal
- Applying for renewal
- I want to renew
- Renew
- Renew Passport

## intent:status_passport
- 2
- Two
- Yeradu
- Number 2
- No 2
- Num 2
- Digit 2
- Status of the passport
- Status
- passport status

## intent:repeat
- Repeated 
- Repeated again 
- Repeated again 
- Please repeat 
- Tell again if you cant understand further 
- Tell me what you said again 
- Tell again 
- Repeat 
- What did you say 
- Repeat 
- What said 
- Repeat 
- and once 
- Speak again 
- Please repeat 
- Please repeat it 
- Say again 
- Repeat once more 
- Can you please repeat 
- Can you repeat 
- Cant hear 
- We didnt hear 
- Please tell me again 
- Please speak again 
- Please repeat it again 
- Sorry I cant hear please repeat it 
- Please speak it again 
- Sorry you didnt get 
- Sorry couldnt hear you repeat 
- Sorry speak again 
- Sorry repeat again 
- Sorry I didnt hear you 
- Please repeat May i cant hear properly 
- Sorry can you say it again 
- Sorry you can repeat it again 
- Can you repeat it 
- Please say it again 
- Please repeat it again 
- Sorry I didnt understand you 
- I wasnt able to hear you 
- I didnt get what you said 
- I wasnt able to understand you 
- What you can tell again 
- What you can say again 
- Can you come again 
- Please say it again 


## intent:outofscope
- Tax payments 
- Run 
- Vote in 
- How many dates are not the next hearings that you should have 
- Well also make it permanent sir 
- Bicycle 
- The rest of it too 
- By the police 
- So kahes answer 
- Close in new session		 
- Employees 
- Part of 
- kyu 
- why 
- what happen 
- me not 


## intent:intent_wrong_number
- Their number has changed 
- Sorry Wrong Number 
- Number is incorrect 
- Wrong Number 
- Wrong number must have been put 
- Sorry you must have dialed the wrong number 
- Sorry that person wont get this number 
- Sorry that persons number is not 
- Sorry theres no person with such a name 
- Sorry the wrong number is wrong
- Sorry number is not correct 
- Sorry incorrect number 
- this is wrong number  
- Check number 
- Absolutely wrong number 
- Check your number 
- Wrong number is called 


## intent:intent_busy
- Later 
- No busy 
- Wrong time 
- Please call later 
- Call me later 
- Im busy 
- Busy calling later 
- Dont call busy 
- still busy 
- Later in call 
- Time is not right now 
- The right time is not exactly the right time 
- Not exactly the right time 
- Yes am quite busy 
- No Im busy 
- Am quite busy  
- Busy 
- Can you call me later 
- Call me later please 
- Call later 
- Call me later 
- right now in mbc call after some time  
- Call in after some time 
- Will you call me after a couple of hours 
- Can you call me tomorrow morning 
- Please call latter 
- Call me later 
- Calling after a week 
- Call after one or two days 
- baad mein 
- nahin bizee hoon 
- galat samay 
- pleez kaal mi letar 
- mujhe baad mein fon karana 
- main bijee hoon 
- bijee hoon baad mein kol karana 
- kol mat karo bijee hoon 
- bijee rahatee hoon 
- kol mein letar 
- samay nahin hai abhee 
- sahee samay nahin hai bilkul sahee samay nahin hai 
- bilkul sahee samay nahin hai 
- yas kaaphee bijee hoon 
- nahin main bijee hoon 
- kaaphee bijee hoon 
- nahin bijee hoon 
- bijee hoon 
- kya aap mujhe baad mein kol kar sakate hain 