import sys
sys.path.append('../')


import datetime
from dateutil import parser
import pytz

timezone = pytz.timezone("Asia/Kolkata")
# present = parser.parse("12/2/2020",dayfirst=True)
# now = datetime.datetime.now()
# print(present - now)

# date_string = parser.parse("12/2/2020")
# now = datetime.datetime.now()
# no_of_days = (date_string - now).days
# print(type(no_of_days))
# if date_string - now <= 0:
#     print("PAST")
# else:
#     print("FUTURE")

def current_hour():
    strings = datetime.datetime.now(timezone).strftime("%Y,%m,%d,%H")
    t = strings.split(',')
    numbers = [ int(x) for x in t ]
    return numbers[3]

def past_or_future(date):
    date_string = parser.parse(date,dayfirst=True)
    now = datetime.datetime.now(timezone)
    date_string = timezone.localize(date_string)
    no_of_days = (date_string - now).days
    if no_of_days <= 0:
        return "PAST"
    else:
        return "FUTURE"

print(past_or_future("02    /02/2020"))

date_string = parser.parse("28/02/2018",dayfirst=True)
print(date_string.strftime("%Y,%m,%d"))

def covert_date_string(input_date):
    arr = input_date.split('-')
    date = arr[0]
    if date[0]=='0':
      date = date.replace('0','')
    m = arr[1]
    year = arr[2]
    if m == '01':
      month = 'january'
    if m == '02':
      month = 'february'
    if m == '03':
      month = 'march'
    if m == '04':
      month = 'april'
    if m == '05':
      month = 'may'
    if m == '06':
      month = 'june'
    if m == '07':
      month = 'july'
    if m == '08':
      month = 'august'
    if m == '09':
      month = 'spetember'
    if m == '10':
      month = 'october'
    if m == '11':
      month = 'november'
    if m == '12':
      month = 'december'
    print(date)
    print(month)
    print(year)

    date = date+ " "+month+" "+year
    print(date)
    return(date)


