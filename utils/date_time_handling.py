"""

Author: Shyam Sunder Kumar
Date: 20th of August, 2k19

This code will help you to handle different date and time validation in hindi.

Functions in this module:

date_handling(date_str) - Check that the given date is correct or not.
time_handling(time_str) - Check that the given time is correct or not.
date_time_handling(date_str,time_str) - check wheather the given date and time together are valid or not
previous_date(today,new_date) - check wheather the given date has passed or not.

"""

import datetime
import re
from dateutil import relativedelta
from dateutil import parser
import sys
import os


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)

def time_replacement(time_str,am_or_pm):
	time_str = time_str.replace(am_or_pm,"")
	time_str = time_str.replace("दोपहर","")
	time_str = time_str.replace("शाम","")
	time_str = time_str.replace("रात","")
	time_str = time_str.replace("सुबह","")
	time_str = time_str.replace("के","")
	time_str = time_str.replace("में","")

	return time_str

"""Time validation

Parameters
----------
nlu_time : str
    The time string returned from ASR

Returns
-------
nlu_hour, nlu_minutes
    Either it will return "TIME ERROR" which means it's not a valid time else it will 
	return the time in hour - nlu_hour and minutes - nlu_minutes.

"""

def time_handling(nlu_time):

	time_str = nlu_time
	nlu_hour = 0
	
	# Handling date and time based on ASR
	# asr output time: 130 pm or 1230 am

	try:
		
		nlu_times = re.findall(r'^0 बजे',time_str)
		if nlu_times[0]=='0 बजे':
			return "TIME ERROR"


	except:
		
		try:

			if ("दोपहर" in time_str or "शाम" in time_str or "रात" in time_str):


				time_str = time_replacement(time_str,"बजे")
				date_time_list = time_str.split(":")

				nlu_hour = int(date_time_list[0])

				try:
					nlu_minutes = int(date_time_list[1])
				except:
					nlu_minutes = 0

				# If 12:30 am don't add 12 else add 12 to hour
				if nlu_hour == 12:
					nlu_hour = 12
				else:
					nlu_hour = nlu_hour+12


				if (nlu_hour>23 or nlu_hour<11) or (nlu_minutes>59 or nlu_minutes<0):
					return "TIME ERROR"
				else:
					return nlu_hour,nlu_minutes


			elif ("सुबह" in time_str):

				time_str = time_replacement(time_str,"बजे")
				date_time_list = time_str.split(":")
				nlu_hour = int(date_time_list[0])

				try:
					nlu_minutes = int(date_time_list[1])
				except:
					nlu_minutes = 0

				# for handling 12:30am etc
				if nlu_hour == 12:
					nlu_hour = nlu_hour - 12

				if (nlu_hour>12 or nlu_hour<0) or (nlu_minutes>59 or nlu_minutes<0):
					return "TIME ERROR"
				else:
					return nlu_hour,nlu_minutes

			#################################################################################################################
			######################### ASR output in the form 1030 am or only morning 1030 i.e without colon #################
			#################################################################################################################

			else:
				
				time_str = time_replacement(time_str,"बजे")
				date_time_list = time_str.split(":")
				nlu_hour = int(date_time_list[0])

				try:
					nlu_minutes = int(date_time_list[1])
				except:
					nlu_minutes = 0

				# for handling 12:30am etc
				if nlu_hour == 12:
					nlu_hour = nlu_hour - 12

				if (nlu_hour>12 or nlu_hour<0) or (nlu_minutes>59 or nlu_minutes<0):
					return "TIME ERROR"
				else:
					return nlu_hour,nlu_minutes

			
		except Exception as e:
			return "TIME ERROR"

		
 
def previous_date(today,new_date):
	try:
		new_date_month = new_date.month
		today_month = today.month
		diff_new_date_today = new_date_month - today_month

		if new_date < today and diff_new_date_today >= -9:
			return "DATE PASSED"
		elif new_date < today and diff_new_date_today < -9:
			year = new_date.year
			new_date = new_date.replace(year = year+1)
			return new_date.isoformat()
		else:
			return new_date.isoformat()
	
	except:
		return "DATE ERROR FROM PREVIOUS DATE"



"""Date validation

Parameters
----------
nlu_date : str
    The date string returned from ASR

Returns
-------
str
    Either it will return "DATE ERROR" which means it's not a valid date else it will 
	return the date as string.
	
"""


def date_handling(nlu_date):
	try:
		date_str = nlu_date

		month_dict = {"जनवरी":"january","फरवरी":"february","मार्च":"march","अप्रैल":"april","मई":"may","जून":"june",
		     "जुलाई":"july","अगस्त":"august","सितम्बर":"september","सितंबर":"september","अक्टूबर":"october","नवम्बर":"november","दिसम्बर":"december"}

		month_list = ["जनवरी","फरवरी","मार्च","अप्रैल","मई","जून","जुलाई","अगस्त","सितम्बर","सितंबर","अक्टूबर"," नवम्बर","दिसम्बर"]

		week_dict = {"रविवार":6,"संडे":6,"मंडे":0,"ट्यूजडे":1,"वेडनसडे":2,"थर्सडे":3,"फ्राइडे":4,"सैटरडे":5,"सोमवार":0,
                                "मंगलवार":1,"बुधवार":2,"बृहस्पतिवार":3,"शुक्रवार":4,"शनिवार":5}

		week_list = ["संडे","मंडे","ट्यूजडे","वेडनसडे","थर्सडे","फ्राइडे","सैटरडे","सोमवार","मंगलवार","बुधवार","बृहस्पतिवार","शुक्रवार","शनिवार","रविवार"]

		today = datetime.datetime.now()
		tomorrow = today + datetime.timedelta(days=1)
		day_after_tomorrow = tomorrow + datetime.timedelta(days=1)


		# Handling today
		if date_str == "आज":
			return "OKAY"


		# Handling tomorrow
		elif ("कल" in date_str):
			return "OKAY"

		# Handling day after tomorrow
		elif date_str =="परसों":
			return "OKAY"

		# Handling dates like 10 days from now
		elif ("दिन बाद") in date_str or ("दिनों बाद") in date_str or ("िन") in date_str or ("दिनों") in date_str:
			return "OKAY"

		elif ("अगले सप्ताह" in date_str or "अगला सप्ताह" in date_str or "नेक्स्ट वीक" in date_str or "अगले हफ्ते" in date_str or "अगला हफ्ते" in date_str or "हफ्ता" in date_str and "बाद" not in date_str):
			return "OKAY"
		
		# Handling any date like: 15th September or 15th of September or October 3rd etc.
		
		# If it starts with any month
		elif any(x in date_str for x in month_list):
			try:

				year_list = date_str.split(" ")

				for i in month_dict.keys():
					if i in date_str:
						date_str = date_str.replace(i,month_dict[i])

				
				# Extract the present year
				
				try:
					present_year = year_list[2]
					date_str = date_str.replace(present_year," ")
					date_str = date_str.replace("  ","")
					present_year = int(present_year)
				except:
					present_year = today.year

				try:
					extract_date_month = datetime.datetime.strptime(date_str, "%d %B")
				except:
					extract_date_month = datetime.datetime.strptime(date_str, "%B %d")
				new_period = extract_date_month.replace(year = present_year)
				new_time = previous_date(today,new_period)
				return new_time
			
			except:
				return "DATE ERROR"


		# Handling dates like next month for ex: 26th of next month
		elif "अगले महीने" in date_str:
			try:

				# Find the date and month of booking		
				date_strs = re.findall(r'[0-9]*',date_str)
				date_list = list(filter(None, date_strs))
				final_date_int = int(date_list[0])

				nextmonth = today + relativedelta.relativedelta(months=1)
				new_period = nextmonth.replace(day = final_date_int,hour = nlu_hour,minute=nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time

			except:
				return "DATE ERROR"

		# Handling dates like this friday , friday etc		

		elif ((any(day_of_week in date_str for day_of_week in week_list)) and (("अगला" not in date_str)
				or ("अगली" not in date_str) or ("नेक्स्ट" not in date_str) or ("सप्ताह" not in date_str) or ("हफ्ते" not in date_str)
				or ("दिन बाद" not in date_str) or ("दिनों बाद" not in date_str) or ("दिनों" not in date_str) or ("िन" not in date_str))):
			return "OKAY"

		# Handling dates like  next friday, next tuesday etc.

		elif ((("अगला" in date_str) or ("अगली" in date_str) or ("नेक्स्ट" in date_str) or ("सप्ताह" not in date_str) or ("हफ्ते" not in date_str)
		or ("दिन बाद" not in date_str) or ("दिनों बाद" not in date_str) or ("दिनों" not in date_str) or ("िन" not in date_str))
				and (any(x in date_str for x in week_list))):
			return "OKAY"

		else:
			try:
				if "of" in date_str:
					date_str = date_str.replace("of","")
					date_str = date_str.replace("  "," ")
					date_str = date_str.replace("   "," ")
				if "th" in date_str:
						date_str = date_str.replace("th","")
				if "st" in date_str:
					date_str = date_str.replace('st',"")
				if "nd" in date_str:
					date_str = date_str.replace('nd',"")
				if "rd" in date_str:
					date_str = date_str.replace('rd',"")

			# Find the date and month of booking
				date_strs = re.findall(r'[0-9]*',date_str)
				date_list = list(filter(None, date_strs))
				final_date_int = int(date_list[0])	
				new_period = today.replace(day = final_date_int)
				new_time = previous_date(today,new_period)
				return new_time

			except Exception as e:
				return "DATE ERROR"
				
	except Exception as e:
		return "DATE ERROR"

"""Date and time validation

Parameters
----------
nlu_date,nlu_time : str
    The date and time string returned from ASR

Returns
-------
str
    Either it will return "DATE ERROR" which means it's not a valid date else it will 
	return the date as string.
	
"""			

def date_time_handling(nlu_date,nlu_time):
	date_str = nlu_date
	time_str = nlu_time

	try:
		month_dict = {"जनवरी":"january","फरवरी":"february","मार्च":"march","अप्रैल":"april","मई":"may","जून":"june",
		     "जुलाई":"july","अगस्त":"august","सितम्बर":"september","सितंबर":"september","अक्टूबर":"october","नवम्बर":"november","दिसम्बर":"december"}

		month_list = ["जनवरी","फरवरी","मार्च","अप्रैल","मई","जून","जुलाई","अगस्त","सितम्बर","सितंबर","अक्टूबर"," नवम्बर","दिसम्बर"]

		week_dict = {"रविवार":6,"संडे":6,"मंडे":0,"ट्यूजडे":1,"वेडनसडे":2,"थर्सडे":3,"फ्राइडे":4,"सैटरडे":5,"सोमवार":0,
                                "मंगलवार":1,"बुधवार":2,"बृहस्पतिवार":3,"शुक्रवार":4,"शनिवार":5}							
		week_list = ["संडे","मंडे","ट्यूजडे","वेडनसडे","थर्सडे","फ्राइडे","सैटरडे","सोमवार","मंगलवार","बुधवार","बृहस्पतिवार","शुक्रवार","शनिवार","रविवार"]

		try:
			nlu_hour,nlu_minutes = time_handling(time_str)
		except:
			return "TIME ERROR"

		today = datetime.datetime.now()
		tomorrow = today + datetime.timedelta(days=1)
		day_after_tomorrow = tomorrow + datetime.timedelta(days=1)

		
		# Handling today
		if date_str == "आज":
			try:
				today = datetime.datetime.now()
				new_period=today.replace(hour=nlu_hour, minute=nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"
		
		# Handling tomorrow
		elif ("कल" in date_str):
			try:
				tomorrow = today + datetime.timedelta(days=1)
				new_period=tomorrow.replace(hour= nlu_hour, minute=nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

		# Handling day after tomorrow
		elif date_str =="परसों" or date_str =="परसों":
			try:
				new_period = day_after_tomorrow.replace(hour = nlu_hour,minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

		# Handling next week
		elif ("अगले सप्ताह" in date_str or "अगला सप्ताह" in date_str or "नेक्स्ट वीक" in date_str or "अगले हफ्ते" in date_str or "अगला हफ्ते" in date_str or "हफ्ता" in date_str and "बाद" not in date_str):
			try:
				next_week = today + datetime.timedelta(days=7)
				new_period = next_week.replace(hour = nlu_hour,minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

		# Handling any date like: 15th September or 15 अगस्त or October 3rd etc.
		
		# If it starts with any month
		elif any(x in date_str for x in month_list):
			try:

				year_list = date_str.split(" ")

				for i in month_dict.keys():
					if i in date_str:
						date_str = date_str.replace(i,month_dict[i])

				
				# Extract the present year
				
				try:
					present_year = year_list[2]
					date_str = date_str.replace(present_year," ")
					date_str = date_str.replace("  ","")
					present_year = int(present_year)
				except:
					present_year = today.year

				try:
					extract_date_month = datetime.datetime.strptime(date_str, "%d %B")
				except:
					extract_date_month = datetime.datetime.strptime(date_str, "%B %d")
				new_period = extract_date_month.replace(year = present_year,hour = nlu_hour,minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			
			except:
				return "DATE ERROR"


		# Handling dates like next month for ex: 26th of next month
		elif "अगले महीने" in date_str:
			try:

				# Find the date and month of booking			
				date_strs = re.findall(r'[0-9]*',date_str)
				date_list = list(filter(None, date_strs))
				final_date_int = int(date_list[0])

				nextmonth = today + relativedelta.relativedelta(months=1)
				new_period = nextmonth.replace(day = final_date_int,hour = nlu_hour,minute=nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time

			except:
				return "DATE ERROR"

		# Handling dates like  next friday, next tuesday etc.

		elif ((("अगला" in date_str) or ("अगली" in date_str) or ("नेक्स्ट" in date_str) and (("सप्ताह" not in date_str) and ("हफ्ते" not in date_str)
				and ("दिन बाद" not in date_str) and ("दिनों बाद" not in date_str) and ("दिनों" not in date_str) and ("िन" not in date_str)))
						and (any(x in date_str for x in week_list))):


			try:
				week_day = date_str.split(" ")
				if len(week_day)==2:
					next_week_date = next_weekday(today, week_dict[week_day[1]]) # 0 = Monday, 1=Tuesday, 2=Wednesday...
				else:
					next_week_date = next_weekday(today,week_dict[week_day[0]])

				week_day_today = today.weekday()
				next_week_day = next_week_date.weekday()

				if week_day_today < next_week_day:
					next_week_date = next_week_date + relativedelta.relativedelta(days=7)
					new_period = next_week_date.replace(hour = nlu_hour,minute = nlu_minutes)
					new_time = previous_date(today,new_period)
					return new_time
				else:

					new_period = next_week_date.replace(hour = nlu_hour,minute = nlu_minutes)
					new_time = previous_date(today,new_period)
					return new_time
			except:
				return "DATE ERROR"

				
				
		# Handling dates like this friday , friday etc		

		elif ((any(day_of_week in date_str for day_of_week in week_list)) and (("अगला" not in date_str)
												 and ("अगली" not in date_str) and ("नेक्स्ट" not in date_str) and ("सप्ताह" not in date_str) and ("हफ्ते" not in date_str)
												 and ("दिन बाद" not in date_str) and ("दिनों बाद" not in date_str) and ("दिनों" not in date_str) and ("िन" not in date_str))):

			try:
				week_day = date_str.split(" ")
				if len(week_day)==2:
					next_week_date = next_weekday(today, week_dict[week_day[1]]) # 0 = Monday, 1=Tuesday, 2=Wednesday...
				else:
					next_week_date = next_weekday(today,week_dict[week_day[0]])
				
				new_period = next_week_date.replace(hour = nlu_hour,minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

		elif "सप्ताह बाद" in date_str or "सप्ताह" in date_str or "हफ्ते बाद" in date_str or "हफ्ते" in date_str or "हफ्ता" in date_str:
			try:
				number_of_weeks_list = date_str.split(" ")
				number_of_week = int(number_of_weeks_list[0])

				next_number_of_days_booking = today + relativedelta.relativedelta(weeks = number_of_week)
				new_period = next_number_of_days_booking.replace(hour = nlu_hour, minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

		elif ("दिन बाद" in date_str or "दिनों बाद" in date_str or "दिनों" in date_str or "िन" in date_str or "सप्ताह बाद" not in date_str 
								or "सप्ताह" not in date_str or "हफ्ते बाद" not in date_str or "हफ्ते" not in date_str or "हफ्ता" not in date_str):
			try:
				number_of_days_list = date_str.split(" ")
				number_of_days = int(number_of_days_list[0])

				next_number_of_days_booking = today + relativedelta.relativedelta(days = number_of_days)
				new_period = next_number_of_days_booking.replace(hour = nlu_hour, minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

		else:
			try:
			# Find the date and month of booking
				date_strs = re.findall(r'[0-9]*',date_str)
				date_list = list(filter(None, date_strs))
				final_date_int = int(date_list[0])

				new_period = today.replace(day = final_date_int,hour = nlu_hour,minute = nlu_minutes)
				new_time = previous_date(today,new_period)
				return new_time
			except:
				return "DATE ERROR"

	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)


if __name__ == "__main__":
	print(time_handling("सुबह 11 बजे"))
	print(date_handling("परसों"))
	print(date_time_handling("परसों","शाम 4 बजे"))