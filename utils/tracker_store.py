from pymongo import MongoClient
import yaml
from logger_conf.logger import logger
import pandas as pd
from datetime import datetime


def load_config():
    conf = ""
    try:
        with open('../configs/config.yml', 'r') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader)
            logger.debug("Loaded the config file successfully")

    except:
        logger.error("Error loading the configuration from the endpoints.yml")
    return conf



def connect_mongo():
    database_config = load_config()
    mongo_host = database_config["mongo_database"]["url"]
    mongo_port = database_config["mongo_database"]["port"]
    mongo_username = database_config["mongo_database"]["username"]
    mongo_password = database_config["mongo_database"]["password"]
    mongo_auth_src = database_config["mongo_database"]["authSource"]
    db = database_config["mongo_database"]["db_name"]
    # mongo_url = "mongodb://"+str(mongo_host) + ":" + str(mongo_port)
    mongo_url = "mongodb://"+str(mongo_username)+":"+str(mongo_password)+"@"+str(mongo_host) + ":" + str(mongo_port)
    print(mongo_url)
    logger.debug("Connecting to the Mongo Database on : {}".format(mongo_url))
    try:
        if (mongo_host is not None) and (mongo_port is not None):
            # mongo_client = MongoClient(mongo_url)
            mongo_client = MongoClient(mongo_url, authSource = mongo_auth_src)
            if mongo_client is not None:
                logger.debug("Connected to the database")
            else:
                logger.error("Could not connect to the Mongo URL !!!")
        else:
            logger.error("Problem when fetching the database details from endpoints.yml !!!") 
    except:
        logger.error("ERROR!!! When trying to connect to Mongo Database")
    return mongo_client[db]


def update_active_call(db, collection, query={}, host='localhost', port=27017, username=None, password=None):
    try:
        # Connect to MongoDB
        db = connect_mongo()
    except Exception as e:
        logger.error("Could not connect to mongo : " + str(e))
    print("Query"+str(query))    
    logger.info("Updating active call info!")
    print("updateing active call info")
    customer_record = db[collection].find_one(query)
    print("Customer record"+str(customer_record))
    #customer_record['active_call'] = 1
    update_seq = {'active_call': 1}
    print("Customer record"+str(update_seq))
    db[collection].update_one(query, {"$set": update_seq})


def read_mongo(db, collection, query={}, host='localhost', port=27017, username=None, password=None, no_id=True):
    """ Read from Mongo and Store into DataFrame """

    try:
    # Connect to MongoDB
        db = connect_mongo()
    except Exception as e:
        logger.error("Could not connect to mongo : "+ str(e))

    # Make a query to the specific DB and Collection
    cursor = db[collection].find(query)

    # Expand the cursor and construct the DataFrame
    df =  pd.DataFrame(list(cursor))

    # Delete the _id
    if no_id:
        del df['_id']

    return df


def number_of_days(due_date, return_days=False, return_month=False):
    today = datetime.today()
    date_format = "%Y/%m/%d"
    due_date = datetime.strptime(due_date, date_format)
    no_of_days = due_date - today
    no_of_days = no_of_days.days
    due_date_month = due_date.strftime("%B")

    if return_days is True:
        return no_of_days

    if return_month is True:
        return due_date_month

    if no_of_days >= -1:
        return "call_1"

    elif no_of_days >= -30 and no_of_days < 0:
        return "call_2"

    elif no_of_days >= -60 and no_of_days < -30:
        return "call_3"

    else:
        return "call_4"


if __name__ == "__main__":
    due_date = "2020/04/30"
    print(number_of_days(due_date, return_month=True))
