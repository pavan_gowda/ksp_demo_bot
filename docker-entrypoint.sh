#! /bin/bash
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`

LOG=OLD_LOGS
CONVO_LOG=logs
if [ ! -d "$LOG" ]; then
    mkdir $LOG
fi

if [ ! -d "$CONVO_LOG" ]; then
    mkdir $CONVO_LOG
fi

pkill -f -9 python3
export LANG=C.UTF-8
# export SANIC_WORKERS=3
cd main/
rasa run actions &
#rasa run --endpoints ../configs/endpoints.yml --credentials ../configs/credentials.yml --connector custom_connector.CustomConnector --model ../models/ --port 5002
python3 rasa_server.py
