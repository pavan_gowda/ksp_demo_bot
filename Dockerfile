FROM gnanivb.azurecr.io/rasa:1.10.16.v1.1
WORKDIR /nlp_server
COPY . .
RUN pip3 install -r requirements.txt
RUN python3 -m spacy download en_core_web_md \
    && python3 -m spacy link en_core_web_md en \
    && export LANG=C.UTF-8 
ENV TWILIO_ACCOUNT_SID=ACfe34dfb4c66ff690a0104d02a4481089
ENV TWILIO_AUTH_TOKEN=4e90399bacd426d050a4c462fb13f04f
ENV TWILIO_NUMBER=whatsapp:+14155238886
EXPOSE 5002
ENTRYPOINT ["/bin/bash", "docker-entrypoint.sh"]
