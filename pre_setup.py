import os
import json
with open("configs/parameters.json", "r", encoding="utf-8") as temp:
    params = json.load(temp)

model_path = params["model_path"]
print(model_path)

model_name = params["model_name"]
print(model_name)

download_s3 = "aws s3 cp model_path .".replace("model_path",model_path)
os.system("mkdir models")
print("downloading from s3")
print(download_s3)
os.system(download_s3)
print("extracting from s3")

os.system("tar xvf model_name".replace("model_name",model_name))
os.system("mv model_name core/ nlu/ fingerprint.json models/".replace("model_name",model_name))
