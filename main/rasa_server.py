#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
sys.path.append("../configs/")

# !/usr/bin/env python3
# -*- coding: utf-8 -*-
from rasa.core.agent import Agent
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.core.utils import EndpointConfig
from custom_connector import TwilioInput
from rasa.core.tracker_store import RedisTrackerStore
import json
import os
from db.tracker_store_connection import load_config
from logger_conf.logger import get_logger
from raven import Client


# chan is the custom channel connector required to handle input and output of the rasa stack

logger = get_logger(__name__)  # name of the module

# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])
with open("../configs/parameters.json","r", encoding="utf-8") as fa:
    parameters = json.load(fa)


## redis connection for trackers
redis_config = load_config()
redis_db_no = redis_config['redis_tracker_store']['db_tracker']


environment = redis_config["server"]["environment"]
if environment == "prod":
    # Reading from environment variables in case of Prod
    redis_host = os.environ["redis_host"]
    redis_port = os.environ["redis_port"]
else:
    redis_host = redis_config["redis_tracker_store"]["url"]
    redis_port = redis_config["redis_tracker_store"]["port"]

account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']
twilio_number = os.environ['TWILIO_NUMBER']

track_store = RedisTrackerStore(domain="default", host=redis_host, port=redis_port, db= redis_db_no)

def serve(port, i):

    try: 
        chan = TwilioInput(account_sid,auth_token,twilio_number)
        # chan = CustomConnector()
        track_store = RedisTrackerStore(domain="default", host=redis_host, port=redis_port, db= redis_db_no)
        act = EndpointConfig(url="http://localhost:5055/webhook")

        logger.info("\nLoading NLU Model\n")
        nlu_interpreter = RasaNLUInterpreter(parameters["nlu"])#,spacy_model_obj=spacy_model_obj)
        logger.info("\nNLU Model Loaded\n")

        agent = Agent.load(parameters["core"], interpreter = nlu_interpreter, action_endpoint=act,tracker_store=track_store)

        logger.info("\n Core Model Loaded and Ready to Serve!\n")
        logger.info("="*100)

        logger.info("Started on port -> " + str(port))
        agent.handle_channels([chan], int(port))
    
    except Exception as e:
        logger.exception("ERROR!!! Not able to load Model: "+str(e))
        client.captureException()

if __name__ == '__main__':
    import datetime
    import multiprocessing
    import os
    import socket
    import subprocess
    import threading
    import time
    from concurrent import futures
    from itertools import cycle
    import psutil
    import requests
    from pytz import timezone
    
    ps = psutil.Process()
    cpul = ps.cpu_affinity()  # CPU affinity list for the process
    no_cpu = 1
    first_port = 5002

    try:
        ports = []
        for i in range(int(no_cpu)):
            portnum = first_port + i
            ports.append(str(portnum))
        port_pool = cycle(ports)

        if (len(cpul) == int(no_cpu)):
            # Exclusiveness in set. Bind to cpu list
            for i in cpul:
                p = multiprocessing.Process(target=serve, args=(int(next(port_pool)), i))
                p.start()
        else:
            # No exclusiveness. Bind to first "no_cpu" cpus.
            for i in range(0, int(no_cpu)):
                p = multiprocessing.Process(target=serve, args=(int(next(port_pool)), i))
                p.start()

    except Exception as e:
        logger.exception("Exception in rasa_server!"+str(e))
        client.captureException()
