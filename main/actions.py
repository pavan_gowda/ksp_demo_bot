#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: actions.py
# Description: Action server to execute custom action ( handling business logic )
# Author: Anjali
# Date: 12-12-2019
import sys
sys.path.append('../')


from datetime import datetime, timedelta
from db.tracker_store_connection import load_config, MongoDB, RedisDB, connect_mongo
from raven import Client
from configs.get_config import get_replaced_message
from utils.text_format import color
from num2words import num2words
from pymongo import MongoClient
import pytz
from pytz import timezone
from logger_conf.logger import get_logger
from logger_conf.logger import CustomAdapter
from datetime import date
from dateutil import parser
import datetime
from rasa_sdk.executor import CollectingDispatcher
import pymongo
import os.path
import pickle
from threading import Thread
import requests
import http.client
import os
from rasa_sdk.events import SlotSet, UserUtteranceReverted, ConversationPaused, Restarted, AllSlotsReset, FollowupAction,  ActionReverted
from rasa_sdk import Action, Tracker
import json
from typing import Text, Dict, Any, List
import logging
from typing import Dict, Text, Any, List, Union, Optional
##import sys
##sys.path.append('../')


timezone = pytz.timezone("Asia/Kolkata")


redis_config = load_config()
redis_db_no = redis_config['redis_tracker_store']['db_mapping']
#redis_db = connect_redis(redis_db_no)

database_config = load_config()
redis_db_obj = RedisDB()


bot_config = load_config()
client = Client(bot_config["raven"]["url"])

logger = get_logger(__name__)
logger = CustomAdapter(logger, {"sender_id": None})

with open("../configs/templates.json", "r", encoding="utf-8") as fa:
    templates = json.load(fa)
# -----------------------------------------------------------------------------------------------------------------------
# colors and text format
# used for color fomatting
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple = color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
end = color.END
##----------------------------------------------------------------------------------------------------------##


class ActionFallback(Action):

    def name(self) -> Text:
        return "action_default_fallback"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # DB updated for stage
        try:
            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()

            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside action_online_dis: inside if redis is NONE")
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            phone_number = cust_info['phone_number']
            full_name = cust_info["full_name"]
            fallback_count = cust_info['fallback_count']

            # to save the user responses and intent -- 29sep 2020
            user_txt = tracker.latest_message['text']
            prev_action = cust_info["stage"]
            logger.info(cyan+" Action Fallback -- previous action --- "+cyan+bold+str(
                prev_action) + " user txt is ---  " + str(user_txt)+end, sender_id=sender_id)

            intent = tracker.latest_message.get("intent", {}).get("name")
            logger.info("Action Fallback : intent identified as: " +
                        str(intent), sender_id=sender_id)
            # if it comes to fallback
            # tracker details
            text = tracker.latest_message['text']
            confidence = tracker.latest_message['intent'].get('confidence')
            text = tracker.latest_message['text']
            #tracker_list = redis_db_obj.tracker_details(tracker, intent, confidence, "action_abt_days")
            logger.info("[Intent Name:---> "+str(intent) +
                        " Confidence is :---> "+str(confidence)+"]")
            #text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            #logger.info("---tracker_list --> "+str(tracker_list))
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            final_dict = cust_info

            conv_dict = {"current_text": user_txt,
                         "intent": intent, "confidence": confidence}
            if prev_action == "":
                conv_dict["previous_action"] = "no_action"
            else:
                conv_dict["previous_action"] = prev_action

            final_dict["call_analysis"] = 1

            logger.info("conv log in fallback ---  " +
                        str(conv_dict), sender_id=sender_id)
            final_dict["fallback_conv"].append(conv_dict)
            logger.info(" final dict is -- " +
                        str(final_dict), sender_id=sender_id)

            is_updated = redis_db_obj.update_stage(tracker, final_dict)
            if is_updated is True:
                logger.info(
                    "Updated stage in redis action fallback here", sender_id=sender_id)
            else:
                logger.info(
                    "failed to update in redis action fallback here", sender_id=sender_id)

            templates_temp = {}

            for key, message in templates.items():
                templates_temp[key] = message

            # Checking for 2 fallback, if bot didn't understand final message is uttered.
            bot_msg = ""
            final_dict['fallback_count'] = 0
            is_updated = redis_db_obj.update_stage(tracker, final_dict)
            if is_updated is True:
                logger.info(
                    "Updated stage in redis -->", sender_id=sender_id)
            else:
                logger.info(
                    "failed to update in redis -->", sender_id=sender_id)

            for event in reversed(tracker.events):
                if event.get("event") == "bot":
                    #logger.debug("Inside Fallback : text is : "+event.get("text"))
                    if templates["utter_fallback"] in event.get("text"):
                        final_dict['fallback_count'] += 1

                        is_updated = redis_db_obj.update_stage(
                            tracker, final_dict)
                        if is_updated is True:
                            logger.info(
                                "Updated stage in redis -->", sender_id=sender_id)
                        else:
                            logger.info(
                                "failed to update in redis -->", sender_id=sender_id)

                        if final_dict['fallback_count'] >= 2:
                            final_dict = {"fallback": "yes",
                                          "fallback_failure": "yes"}
                            is_updated = redis_db_obj.update_stage(
                                tracker, final_dict)
                            if is_updated is True:
                                logger.info(
                                    "Updated stage in redis -->", sender_id=sender_id)
                            else:
                                logger.info(
                                    "failed to update in redis -->", sender_id=sender_id)

                            dispatcher.utter_message(
                                templates["utter_bot_not_understand"])
                            return [Restarted()]
                            # Call is Ended EOC
                        else:
                            final_dict['fallback_count'] += 1
                            is_updated = redis_db_obj.update_stage(
                                tracker, final_dict)
                            if is_updated is True:
                                logger.info(
                                    "Updated stage in redis -->", sender_id=sender_id)
                            else:
                                logger.info(
                                    "failed to update in redis -->", sender_id=sender_id)

                            bot_msg = event.get("text")
                            logger.info(
                                cyan+"BOT MESSAGE IN INCREAMENT:"+cyan+bold+str(bot_msg)+end)
                            for template_key, template_message in templates_temp.items():
                                if event.get("text") == template_message:
                                    short_key = template_key + "_short"
                                    if short_key in templates_temp:
                                        bot_msg = templates_temp[short_key]
                                        break
                                    else:
                                        bot_msg = templates_temp[template_key]
                                        break
                            #bot_msg = last_bot_message
                            break
                    elif event.get("text") in templates_temp.values():
                        # replace bot message here\
                        final_dict['fallback_count'] += 1
                        is_updated = redis_db_obj.update_stage(
                            tracker, final_dict)
                        if is_updated is True:
                            logger.info(
                                "Updated stage in redis -->", sender_id=sender_id)
                        else:
                            logger.info(
                                "failed to update in redis -->", sender_id=sender_id)

                        bot_msg = event.get("text")
                        logger.debug(
                            cyan+"Bot message in the elif block of fallback is : "+cyan+bold+str(bot_msg)+end)
                        for template_key, template_message in templates_temp.items():
                            logger.debug(
                                cyan+"Template message in the elif block of fallback is : "+cyan+bold+str(template_message)+end)
                            if event.get("text") == template_message:
                                short_key = template_key + "_short"
                                if short_key in templates_temp:
                                    bot_msg = templates_temp[short_key]
                                    break
                                else:
                                    bot_msg = templates_temp[template_key]
                                    break
                        break
                    else:
                        bot_msg = event.get("text")
                        logger.debug(
                            cyan+"Bot message in the else block of fallback is : "+cyan+bold+str(bot_msg)+end)
                        for template_key, template_message in templates_temp.items():
                            #logger.debug("Template message in the else block of fallback is : "+str(template_message))
                            if event.get("text") == template_message:
                                short_key = template_key + "_short"
                                if short_key in templates_temp:
                                    bot_msg = templates_temp[short_key]
                                    break
                                else:
                                    bot_msg = templates_temp[template_key]
                                    break
                        #bot_msg = last_bot_message
                        final_dict['fallback_count'] += 1
                        is_updated = redis_db_obj.update_stage(
                            tracker, final_dict)
                        if is_updated is True:
                            logger.info(
                                "Updated stage in redis -->", sender_id=sender_id)
                        else:
                            logger.info(
                                "failed to update in redis -->", sender_id=sender_id)

                        break
            final_dict['fallback_count'] = 0
            is_updated = redis_db_obj.update_stage(tracker, final_dict)
            if is_updated is True:
                logger.info(
                    "Updated stage in redis -->", sender_id=sender_id)
            else:
                logger.info(
                    "failed to update in redis -->", sender_id=sender_id)

            for event in reversed(tracker.events):
                if event.get("event") == "bot":
                    logger.info(cyan+"Inside Fallback : text is : " +
                                cyan+bold+str(event.get("text"))+end)
                    if templates["utter_fallback"] in event.get("text"):
                        final_dict['fallback_count'] += 1
                        is_updated = redis_db_obj.update_stage(
                            tracker, final_dict)
                        if is_updated is True:
                            logger.info(
                                "Updated stage in redis -->", sender_id=sender_id)
                        else:
                            logger.info(
                                "failed to update in redis -->", sender_id=sender_id)

                        if final_dict['fallback_count'] >= 2:
                            final_dict = {"fallback": "yes",
                                          "fallback_failure": "yes"}

                            is_updated = redis_db_obj.update_stage(
                                tracker, final_dict)
                            if is_updated is True:
                                logger.info(
                                    "Updated stage in redis -->", sender_id=sender_id)
                            else:
                                logger.info(
                                    "failed to update in redis -->", sender_id=sender_id)

                            dispatcher.utter_message(
                                templates["utter_bot_not_understand"])
                            return [Restarted()]

            if bot_msg == "":
                logger.info(red+"inside BOT MESSAGE EMPTY"+end)
                final_dict = {"stage": "inital_message_repeat"}
                is_updated = redis_db_obj.update_stage(tracker, final_dict)
                if is_updated is True:
                    logger.info(
                        "Updated stage in redis -->", sender_id=sender_id)
                else:
                    logger.info(
                        "failed to update in redis -->", sender_id=sender_id)

                dispatcher.utter_message(
                    templates["initial_message"].replace("full_name", name))
            else:
                if templates["utter_fallback"] in bot_msg:
                    logger.info(
                        cyan+"IF <utter_fallback> present in BOT MESSAGE:"+cyan+bold+str(bot_msg)+end)
                    dispatcher.utter_message(bot_msg)
                else:
                    logger.info(
                        cyan+"IF <utter_fallback> present not in BOT MESSAGE:"+cyan+bold+str(bot_msg)+end)
                    dispatcher.utter_message(
                        templates["utter_fallback"] + '~ ' + bot_msg)

            return [UserUtteranceReverted()]
        except Exception as e:
            logger.exception("action_fallback : fallback failed for " +
                             str(sender_id) + "Exception is : "+str(e), sender_id=sender_id)
            client.captureException()
            return [UserUtteranceReverted()]


class ActionRepeat(Action):

    def name(self) -> Text:
        return "action_repeat"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:

            sender_id = tracker.current_state()["sender_id"]
            logger.info("---Entering action_repeat--- ", sender_id=sender_id)

            logger.info("After Redis Fetch : ", sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside action_online_dis: inside if redis is NONE")
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]
            phone_number = cust_info['phone_number']

            logger.info("phone_number is : " +
                        str(phone_number), sender_id=sender_id)
            name = cust_info['full_name']

            bot_msg = ""

            final_dict = {"repeat": "yes"}
            is_updated = redis_db_obj.update_stage(tracker, final_dict)
            if is_updated is True:
                logger.info(
                    "Updated stage in redis -->", sender_id=sender_id)
            else:
                logger.info(
                    "failed to update in redis -->", sender_id=sender_id)

            for event in reversed(tracker.events):
                if event.get("event") == "bot":
                    bot_not_answered = False
                    bot_msg = event.get("text")
                    break
                else:
                    bot_not_answered = True

            repeat = cust_info['repeat_count']
            for event in tracker.events:

                if event.get("event") == "user" and \
                        event.get("parse_data")["intent"]["name"] == "intent_repeat":
                    repeat += 1
                    logger.info("Sender ID:" + str(sender_id) +
                                ", Phone Number:" + str(phone_number) + ", ActionRepeat: increment"+str(repeat), sender_id=sender_id)

                    # update fallback count
                    final_dict = {"repeat_count": repeat}

                    is_updated = redis_db_obj.update_stage(tracker, final_dict)
                    if is_updated is True:
                        logger.info(
                            "Updated stage in redis -->", sender_id=sender_id)
                    else:
                        logger.info(
                            "failed to update in redis -->", sender_id=sender_id)

                elif event.get("event") == "user" and event.get("parse_data")["intent"]["name"] != "intent_repeat":
                    repeat = 0

            if repeat > 2:
                logger.info("Sender ID:" + str(sender_id) +
                            ", Phone Number:" + str(phone_number) + ", ActionRepeat: Repeat Action", sender_id=sender_id)

                logger.info("Agent flag is no", sender_id=sender_id)

                dispatcher.utter_message(
                    templates["utter_bot_not_understand"])

                return []
                # Call is Ended EOC
            else:

                logger.info("Sender ID:" + str(sender_id) +
                            ", Phone Number:" + str(phone_number) + ", ActionRepeat: Repeat Action", sender_id=sender_id)
                if bot_not_answered:
                    final_dict = {"stage": "repeat", "STAGE_CODE": "DSCN",
                                  "call_comp_flag": "no"}
                    is_updated = redis_db_obj.update_stage(tracker, final_dict)
                    if is_updated is True:
                        logger.info(
                            "Updated stage in redis -->", sender_id=sender_id)
                    else:
                        logger.info(
                            "failed to update in redis -->", sender_id=sender_id)

                    logger.info("bot did not utter anything",
                                sender_id=sender_id)
                    dispatcher.utter_message(templates["initial_message"].replace(
                        "full_name", name))
                    return [UserUtteranceReverted()]

                else:
                    logger.info("bot_not_answered is false",
                                sender_id=sender_id)
                    dispatcher.utter_message(bot_msg)
                    return [UserUtteranceReverted(), ActionReverted()]

        except Exception as e:
            logger.error("Exception in action repeat --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_callback(Action):

    def name(self):
        return "action_callback"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]
            utter_msg = templates["utter_callback"]
            utter_msg = get_replaced_message(utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_greet(Action):

    def name(self):
        return "action_greet"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["initial_message"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]



class action_user_status(Action):

    def name(self):
        return "action_user_status"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_user_status"]
            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_user_detail1(Action):

    def name(self):
        return "action_user_detail1"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_user_detail1"]
            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_user_detail2(Action):

    def name(self):
        return "action_user_detail2"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_user_detail2"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)
            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_user_detail3(Action):

    def name(self):
        return "action_user_detail3"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_user_detail3"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_user_register(Action):

    def name(self):
        return "action_user_register"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_user_register"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_user_existing(Action):

    def name(self):
        return "action_user_existing"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_user_existing"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_theft_status(Action):

    def name(self):
        return "action_theft_status"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_theft_status"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_passport_user(Action):

    def name(self):
        return "action_passport_user"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_passport_user"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_passport_first(Action):

    def name(self):
        return "action_passport_first"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_passport_first"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_passport_existing(Action):

    def name(self):
        return "action_passport_existing"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_passport_existing"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]


class action_passport_renewal(Action):

    def name(self):
        return "action_passport_renewal"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_passport_renewal"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]



class action_passport_status(Action):

    def name(self):
        return "action_passport_status"

    def run(self, dispatcher, tracker, domain):

        try:

            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')
            logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent) +
                        darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)

            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            sender_id = tracker.current_state()["sender_id"]
            logger.info("Executing action : {}".format(
                action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside {}: -> if redis is NONE".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            utter_msg = templates["utter_passport_status"]

            utter_msg = get_replaced_message(
                utter_msg, sender_id, cust_info)

            dispatcher.utter_message(utter_msg)
            return [FollowupAction("action_listen")]

        except Exception as e:
            logger.error("Exception in action date_time --> " +
                         str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]



class ActionWrongNumber(Action):
    """Action to utter message for wrong number
    This action is called whenever user says it is wrong number

    Args:
        Action : Inherits action class of the rasa core
    """

    def name(self):
        return "action_wrong_number"

    def run(self, dispatcher, tracker, domain):

        try:

            sender_id = tracker.current_state()["sender_id"]
            logger.info("---Entering action_wrong_number--- ",
                        sender_id=sender_id)

            logger.info("After Redis Fetch : ", sender_id=sender_id)
            intent = tracker.latest_message.get("intent", {}).get("name")

            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(
                    "inside action_online_dis: inside if redis is NONE")
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            if intent == 'intent_dont_call':

                final_dict = {"stage": "dont_call", "STAGE_CODE": "DND"}

                is_updated = redis_db_obj.update_stage(tracker, final_dict)
                if is_updated is True:
                    logger.info(
                        "Updated stage in redis -->", sender_id=sender_id)
                else:
                    logger.info(
                        "failed to update in redis -->", sender_id=sender_id)

                dispatcher.utter_message(templates["utter_wrong_number"])
                return[]

            else:

                final_dict = {"stage": "wrong_number", "STAGE_CODE": "WRNG"}

                is_updated = redis_db_obj.update_stage(tracker, final_dict)
                if is_updated is True:
                    logger.info(
                        "Updated stage in redis -->", sender_id=sender_id)
                else:
                    logger.info(
                        "failed to update in redis -->", sender_id=sender_id)

                dispatcher.utter_message(templates["utter_wrong_number"])
                return[]

        except Exception as e:
            logger.error(
                "Exception in sending alert-->  action wrong_number"+str(e), sender_id=sender_id)
            client.captureException()
            return [FollowupAction("action_default_fallback")]
