# -*- coding: utf-8 -*-
# Author: Roy
from sanic.request import Request
from sanic.response import HTTPResponse
from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client
from rasa.core.channels.channel import InputChannel
from rasa.core.channels.channel import UserMessage, OutputChannel


from logger_conf.logger import get_logger, CustomAdapter
# from raven import Client
from db.tracker_store_connection import load_config, MongoDB, RedisDB
from threading import Thread
from datetime import date, timedelta
from rasa.core import utils
from rasa.core.channels.channel import CollectingOutputChannel
from rasa.core.channels.channel import InputChannel
from rasa.core.channels.channel import UserMessage, QueueOutputChannel
import rasa.utils.endpoints
from sanic import response
from sanic.response import HTTPResponse
from sanic.request import Request
from sanic import Sanic, Blueprint, response
from asyncio import Queue, CancelledError
from operator import itemgetter
from typing import Text, List, Dict, Any, Optional, Callable, Iterable, Awaitable
from datetime import datetime, timezone
import time
import pickle
import pytz
import logging
import json
import inspect
import asyncio
import sys
sys.path.append('../')
#from utils.sms import send_message
#from utils.email_alert import send_mail as alert
#from utils.email_alert import send_converstaion_logs

# name of the module for logging
logger = get_logger(__name__)
logger = CustomAdapter(logger, {"sender_id": None})
# time in IST
utc_dt = datetime.now(timezone.utc)
IST = pytz.timezone('Asia/Kolkata')


# fetch the messages from the template file
with open("../configs/templates.json", "r", encoding="utf-8") as temp:
    templates = json.load(temp)

# loading endpoints
gen_config = load_config()
# redis connection
# client = Client(gen_config["raven"]["url"])

mongo_conn = MongoDB()
redis_db_obj = RedisDB()


class TwilioOutput(Client, OutputChannel):
    """Output channel for Twilio"""

    @classmethod
    def name(cls) -> Text:
        return "twilio"

    def __init__(
        self,
        account_sid: Optional[Text],
        auth_token: Optional[Text],
        twilio_number: Optional[Text],
    ) -> None:
        super().__init__(account_sid, auth_token)
        logger.info(str(account_sid))
        logger.info(str(auth_token))
        logger.info(str(twilio_number))
        # account_sid=account_sid[:len(account_sid)-1:]
        self.account_sid = account_sid
        logger.info(str(account_sid))
        self.twilio_number = twilio_number
        self.send_retry = 0
        self.max_retry = 5

    async def _send_message(self, message_data: Dict[Text, Any]):
        logger.info('......')
        message = None
        try:
            while not message and self.send_retry < self.max_retry:
                logger.info('------------------------------')
                logger.info(str(message_data))
                message = self.messages.create(**message_data)
                logger.info('--------------------------------')
                self.send_retry += 1
        except TwilioRestException as e:
            logger.exception("Something went wrong " + str(e))
        finally:
            self.send_retry = 0

        if not message and self.send_retry == self.max_retry:
            logger.error(
                "Failed to send message. Max number of retires exceeded.")

        return message

    async def send_text_message(
        self, recipient_id: Text, text: Text, **kwargs: Any
    ) -> None:
        """Sends text message"""

        message_data = {"to": recipient_id, "from_": self.twilio_number}
        for message_part in text.strip().split("\n\n"):
            message_data.update({"body": message_part})
            await self._send_message(message_data)

    async def send_image_url(
        self, recipient_id: Text, image: Text, **kwargs: Any
    ) -> None:
        """Sends an image."""

        message_data = {
            "to": recipient_id,
            "from_": self.twilio_number,
            "media_url": [image],
        }
        await self._send_message(message_data)

    async def send_custom_json(
        self, recipient_id: Text, json_message: Dict[Text, Any], **kwargs: Any
    ) -> None:
        """Send custom json dict"""

        json_message.setdefault("to", recipient_id)
        if not json_message.get("media_url"):
            json_message.setdefault("body", "")
        if not json_message.get("messaging_service_sid"):
            json_message.setdefault("from_", self.twilio_number)

        await self._send_message(json_message)


class TwilioInput(InputChannel):
    """Twilio input channel"""

    @classmethod
    def name(cls) -> Text:
        return "twilio"

    @classmethod
    def from_credentials(cls, credentials: Optional[Dict[Text, Any]]) -> InputChannel:
        if not credentials:
            cls.raise_missing_credentials_exception()

        return cls(
            credentials.get("account_sid"),
            credentials.get("auth_token"),
            credentials.get("twilio_number"),
        )

    def __init__(
        self,
        account_sid: Optional[Text],
        auth_token: Optional[Text],
        twilio_number: Optional[Text],
        debug_mode: bool = True,
    ) -> None:
        self.account_sid = account_sid
        self.auth_token = auth_token
        self.twilio_number = twilio_number
        logger.info(str(account_sid))
        logger.info(str(auth_token))
        logger.info(str(twilio_number))
        self.debug_mode = debug_mode

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[Any]]
    ) -> Blueprint:
        twilio_webhook = Blueprint("twilio_webhook", __name__)

        @twilio_webhook.route("/", methods=["GET"])
        async def health(_: Request) -> HTTPResponse:
            return response.json({"status": "ok"})

        @twilio_webhook.route("/webhook", methods=["POST"])
        async def message(request: Request) -> HTTPResponse:
            sender = request.form.get("From", None)
            mobile = 0
            if sender is not None:
                sender2 = sender.replace('whatsapp:', '').replace('+91', '', 1)
                if sender2.isdigit():
                    mobile = int(sender2)

            text = request.form.get("Body", None)

            print("request is : ", str(request.form))
            logger.info("custom_connector: phone_number: "+str(mobile))
            logger.info("custom_connector: text: "+str(text))

            if text is None or text == '':
                text = 'How may i help you?'

            # should_use_stream = rasa.utils.endpoints.bool_arg(
            #     request, "stream", default=False
            # )
            # Logic to fetch the data from the mongo and push it to redis
            try:
                # Connect to the Redis Database and check if the number is already preset or else load the mongo Database and import to Redis
                if len(str(mobile)) == 10:
                    redis_data = redis_db_obj.redis_get_dict_value(str(mobile))
                    if redis_data is None:
                        logger.info(f"Session Start")
                        # Load the Database configuration

                        # add active_call 1
                        condition = {"phone_number": mobile}
                        q1 = condition  # filter condition to find the document
                        # 2nd query condition optional, setting id column to 0 , or if only certain fields are req  OPTIONAL
                        q2 = {"_id": 0}
                        input_collection = mongo_conn.mongo_get_collection_data(
                            col_type="input", q1=q1, q2=q2)

                        if input_collection is not None:
                            # q1 = condition # filter condition to find the document
                            # q2 = {"_id": 0}  # 2nd query condition optional, setting id column to 0 , or if only certain fields are req  OPTIONAL
                            # input_collection = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2)
                            logger.info(
                                "\n Input collection obtained --> "+str(input_collection)+"\n")
                            input_collection['stage'] = 'welcome_stage'
                            input_collection['call_analysis'] = 0
                            input_collection['Next_Action'] = ''
                            input_collection['fallback_failure'] = 'no'
                            input_collection['repeat_failure'] = 'no'
                            input_collection['fallback_conv'] = []
                            input_collection['conversation_log'] = []
                            input_collection['STAGE_CODE'] = 'DSCN'
                            input_collection['flag'] = int(0)
                            input_collection['fallback_count'] = int(0)
                            input_collection['repeat_count'] = int(0)
                            input_collection['callback_count'] = int(0)
                            input_collection['language_count'] = int(0)
                            input_collection['TTA'] = ""
                            input_collection['tracker_log'] = []

                            val = redis_db_obj.redis_set_dict_value_and_expiry(str(
                                mobile), input_collection, 10)  # SETTING THE DICTIONARY VALUES USING PICKLE
                            logger.info(
                                "return val from redis set dict val --> "+str(val))
                            logger.info(
                                "========================================================")

                        else:
                            logger.info(
                                "Insert New record for phone number: "+str(mobile))
                            existing_details = {
                                "active_call": 1,
                                "answered_seq": 0,
                                "sequence_number": 0,
                                "no_answer_seq": 0,
                                "language": "Hindi",
                                "input_date": "07/10/20",

                                "OrderState": "SELLER_ORDER_SHIPPED",
                                "ReturnState": "",
                                "RVPState": "",
                                "CreditEligibility": "0",

                                "OrderETABreached": 0,
                                "OrderHasRevisedETA": 0,
                                "OrderRevisedETABreached": 0,
                                "OrderHasPrepayment": 0,

                                "HasCreditPayment": 0,
                                "Refund_Initiated_Breached":  0,
                                "Returns_Action_TAT_Breach":  0,
                                "Verification_TAT_Breached": 0,
                                "RefundAmount": 5000,

                                "RVP_Required":  0,
                                "KYC_Documents_Uploaded": 0,

                                "full_name": "gnani",
                                "phone_number": int(mobile),
                                "duplicate": 1,
                                "gnani_test": "gnani_test",
                                "Next_Action": "",
                                "call_comp_flag": "",
                                "call_status": "",
                                "stage": "",
                                "sts": "",
                                "flow_id": "",
                                "prepaymentAmount": "6000",
                                "oldDeliveryDate": "27 june 2021",
                                "newDeliveryDate": "10 july 2021",
                                "deliveryDate": "27 june 2021",
                                "return_Action_TAT": "10 july 2021",
                                "Order_ID": "1020",
                                "Last_Reattempt_Reason": "user not reachable",
                                "Order_Delivery_Rescheduled_Date": "10 july 2021",
                                "RTO_Reason": "delivery agent tried 3 times",
                                "Delivery_promise_date": "10 july 2021",
                                "promiseDateFor2days": "12 july 2021",
                                "Uploaded_Date_plus2": "6 july 2021"

                            }
                            existing_details['stage'] = 'welcome_stage'
                            existing_details['call_analysis'] = 0
                            existing_details['Next_Action'] = ''
                            existing_details['fallback_failure'] = 'no'
                            existing_details['repeat_failure'] = 'no'
                            existing_details['fallback_conv'] = []
                            existing_details['conversation_log'] = []
                            existing_details['STAGE_CODE'] = 'DSCN'
                            existing_details['flag'] = int(0)
                            existing_details['fallback_count'] = int(0)
                            existing_details['repeat_count'] = int(0)
                            existing_details['callback_count'] = int(0)
                            existing_details['language_count'] = int(0)
                            existing_details['TTA'] = ""
                            existing_details['tracker_log'] = []

                            condition = {"phone_number": mobile}
                            is_inserted = mongo_conn.mongo_update(
                                col_type="input", mode="insert", record=existing_details)

                            logger.info("Updated Call Status Successfully")

                            val = redis_db_obj.redis_set_dict_value_and_expiry(str(
                                mobile), existing_details, 10)
                            logger.info(
                                "value inserted in mongo and redis --> "+str(val))
                            logger.info(
                                "========================================================")

                    else:
                        logger.info(
                            "Record Already Exists! Skipping the insert!")
                else:
                    logger.info(f'Phone Number is not 10 digit')
            except Exception as e:
                logger.exception(
                    "custom_connector: Error!!! when fetching database and setting in redis:"+str(e))

            out_channel = self.get_output_channel()

            logger.info(f'after output channel')
            if sender is not None and message is not None:
                metadata = self.get_metadata(request)
                try:
                    # @ signs get corrupted in SMSes by some carriers
                    text = text.replace("¡", "@")
                    await on_new_message(
                        UserMessage(
                            text,
                            out_channel,
                            sender,
                            input_channel=self.name(),
                            metadata=metadata,
                        )
                    )
                except Exception as e:
                    logger.error(
                        f"Exception when trying to handle message.{e}")
                    logger.info(e, exc_info=True)
                    if self.debug_mode:
                        raise
                    pass
            else:
                logger.info("Invalid message")
            logger.info(str(response.text("", status=204)))
            logger.info(f'Inserting into the DB')
            # call status cases

            return response.text("", status=204)

        return twilio_webhook

    def get_output_channel(self) -> OutputChannel:
        logger.info(str(self.account_sid))
        # if globals()['global_val']==0:
        #     self.account_sid=self.account_sid[:len(self.account_sid)-1:]
        #     globals()['global_val']+=1
        logger.info(str(self.auth_token))
        logger.info(str(self.twilio_number))
        return TwilioOutput(self.account_sid, self.auth_token, self.twilio_number)
